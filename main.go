package main

import (
	"bytes"
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"

	"golang.org/x/crypto/ocsp"
)

// July 2012 is the CAB Forum deadline for when CAs must stop issuing certificates valid for more than 5 years.
var jul2012 = time.Date(2012, time.July, 1, 0, 0, 0, 0, time.UTC).Add(-1 * time.Nanosecond)

// April 2015 is the CAB Forum deadline for when CAs must stop issuing certificates valid for more than 39 months.
var apr2015 = time.Date(2015, time.April, 1, 0, 0, 0, 0, time.UTC).Add(-1 * time.Nanosecond)

// September 2020 is the Apple deadline for when CAs must stop issuing certificates valid for more than 13 months.
var sep2020 = time.Date(2020, time.September, 1, 0, 0, 0, 0, time.UTC).Add(-1 * time.Nanosecond)

func main() {
	var checkServerHost string
	var checkServerPort string
	var timing bool
	flag.StringVar(&checkServerHost, "host", "ngerakines.me", "The server to connect to.")
	flag.StringVar(&checkServerPort, "port", "443", "The port to connect to.")
	flag.BoolVar(&timing, "timing", false, "Output timing information.")

	flag.Parse()

	fmt.Println("HOST", checkServerHost)
	fmt.Println("PORT", checkServerPort)

	if err := runCheck(checkServerHost, checkServerPort, timing); err != nil {
		fmt.Println("ERROR", err)
	}
}

type connectionStateProvider interface {
	ConnectionState() tls.ConnectionState
	io.Closer
}

type checkerDialer interface {
	Dial(ctx context.Context, host, port, ip string) (connectionStateProvider, error)
}

type tlsCheckerDialer struct {
	Timeout time.Duration
}

func runCheck(host, port string, timing bool) error {
	if timing {
		start := time.Now()
		defer func() {
			elapsed := time.Since(start)
			fmt.Println("TIME CHECK", elapsed)
		}()
	}
	ctx := context.Background()
	now := time.Now()

	ips, err := resolveIPs(host, timing)
	if err != nil {
		return err
	}
	if len(ips) == 0 {
		return fmt.Errorf("host must resolve to one or more IP address")
	}

	for _, ip := range ips {
		fmt.Println("RESOLVED", ip)
	}

	ipCerts, err := collectedResolvedCertificates(ctx, host, port, ips, timing)
	if err != nil {
		return err
	}

	if err = verifyCertificates(ipCerts); err != nil {
		return err
	}

	certificates := ipCerts[ips[0]]

	if err := certificates[0].VerifyHostname(host); err != nil {
		return err
	}

	if err := verifyServerCertificate(host, certificates); err != nil {
		return err
	}

	if certificates[0].NotAfter.Before(now) {
		return fmt.Errorf("certificate expired")
	}

	if len(certificates) == 1 {
		fmt.Println("WARNING Certificate change has one certificate.")
	}

	if certificates[0].IsCA {
		fmt.Println("WARNING Certificate Authority")
	}

	if len(certificates) > 1 {
		err = isCertificateRevokedByOCSP(ctx, certificates[0], certificates[1], timing)
		if err != nil {
			fmt.Println("WARNING", err)
		}
	}

	if _, _, ok := isValidExpiry(certificates[0]); !ok {
		return err
	}

	if certificates[0].NotAfter.Before(now.AddDate(0, 0, 1)) {
		return fmt.Errorf("certificate expires tomorrow: %s", certificates[0].NotAfter.String())
	}

	if certificates[0].NotAfter.Before(now.AddDate(0, 0, 7)) {
		fmt.Println("WARNING certificate expires within 7 days:", certificates[0].NotAfter.String())
	} else if certificates[0].NotAfter.Before(now.AddDate(0, 0, 30)) {
		fmt.Println("WARNING certificate expires within 30 days:", certificates[0].NotAfter.String())
	} else if certificates[0].NotAfter.Before(now.AddDate(0, 0, 60)) {
		fmt.Println("WARNING certificate expires within 60 days:", certificates[0].NotAfter.String())
	}

	if len(certificates[0].Subject.Names) == 0 {
		fmt.Println("WARNING no subject")
	} else {
		if certificates[0].Subject.CommonName == "" {
			fmt.Println("WARNING no common name")
		}
	}

	for _, cert := range certificates {
		switch cert.SignatureAlgorithm {
		case x509.MD2WithRSA, x509.MD5WithRSA, x509.SHA1WithRSA, x509.DSAWithSHA1, x509.ECDSAWithSHA1:
			return fmt.Errorf("bad signature algorithm: %s", cert.SignatureAlgorithm)
		}
		// fmt.Println("EXTRA", i, "sigalg", cert.SignatureAlgorithm)
		// fmt.Println("EXTRA", i, "pubkeyalg", cert.PublicKeyAlgorithm)
	}

	uniqueNames := make(map[string]bool)
	for _, certificate := range certificates {
		for _, name := range certificate.DNSNames {
			if name == host {
				continue
			}
			uniqueNames[name] = true
		}
	}
	for name := range uniqueNames {
		fmt.Println("NAME", name)
	}

	return nil
}

func resolveIPs(host string, timing bool) ([]string, error) {
	if timing {
		start := time.Now()
		defer func() {
			elapsed := time.Since(start)
			fmt.Println("TIME RESOLVE", elapsed)
		}()
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rch := make(chan []string, 1)
	ech := make(chan error, 1)

	go func() {
		addrs, err := net.DefaultResolver.LookupIPAddr(ctx, host)
		if err != nil {
			ech <- err
			return
		}
		ips := make([]string, 0)
		for _, ia := range addrs {
			ipStr := ia.IP.String()
			if isIPv4(ipStr) {
				ips = append(ips, ipStr)
			}
		}
		rch <- ips
	}()

	select {
	case ips := <-rch:
		return ips, nil
	case err := <-ech:
		return nil, err
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}

func isIPv4(address string) bool {
	return strings.Count(address, ":") < 2
}

func collectedResolvedCertificates(ctx context.Context, host, port string, ips []string, timing bool) (map[string][]*x509.Certificate, error) {
	results := make(map[string][]*x509.Certificate)

	dialer := tlsCheckerDialer{
		Timeout: 10 * time.Second,
	}

	for _, ip := range ips {
		var connStart time.Time
		if timing {
			connStart = time.Now()
		}
		conn, err := dialer.Dial(ctx, host, port, ip)
		if err != nil {
			return nil, err
		}
		state := conn.ConnectionState()
		certificates := state.PeerCertificates

		if err = conn.Close(); err != nil {
			return nil, err
		}

		results[ip] = certificates
		if timing {
			fmt.Println("TIME COLLECT", ip, time.Since(connStart))
		}
	}

	return results, nil
}

func (cd tlsCheckerDialer) Dial(ctx context.Context, host, port, ip string) (connectionStateProvider, error) {
	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
		Time:               func() time.Time { return time.Now().UTC() },
	}
	return tls.DialWithDialer(&net.Dialer{Timeout: 15 * time.Second}, "tcp", net.JoinHostPort(ip, port), tlsConfig)
}

func verifyCertificates(certs map[string][]*x509.Certificate) error {

	var firstFingerprints []string

	for _, certificates := range certs {
		if len(certs) == 0 {
			return fmt.Errorf("no certificates found")
		}

		var fingerprints []string
		for _, certificate := range certificates {
			fingerprintHash := sha256.Sum256(certificate.Raw)
			fingerprints = append(fingerprints, hex.EncodeToString(fingerprintHash[:]))
		}

		if firstFingerprints == nil {
			firstFingerprints = fingerprints
			continue
		}

		if firstFingerprints[0] != fingerprints[0] {
			return fmt.Errorf("different certificates found")
		}

	}
	if len(firstFingerprints) > 0 {
		fmt.Println("FINGERPRINT", firstFingerprints[0])
	}

	return nil
}

func isCertificateRevokedByOCSP(ctx context.Context, clientCert, issuerCert *x509.Certificate, timing bool) error {
	if timing {
		start := time.Now()
		defer func() {
			elapsed := time.Since(start)
			fmt.Println("TIME OCSP", elapsed)
		}()
	}

	if len(clientCert.OCSPServer) == 0 {
		return nil
	}

	ocspURL, err := url.Parse(clientCert.OCSPServer[0])
	if err != nil {
		return err
	}

	opts := &ocsp.RequestOptions{Hash: crypto.SHA1}
	buffer, err := ocsp.CreateRequest(clientCert, issuerCert, opts)
	if err != nil {
		return err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, ocspURL.String(), bytes.NewBuffer(buffer))
	if err != nil {
		return err
	}

	request.Header.Add("Content-Type", "application/ocsp-request")
	request.Header.Add("Accept", "application/ocsp-response")
	request.Header.Add("host", ocspURL.Host)
	request.Header.Add("User-Agent", "sslhound-check")

	httpClient := &http.Client{
		Timeout: 15 * time.Second,
	}
	httpResponse, err := httpClient.Do(request)
	if err != nil {
		return err
	}
	defer httpResponse.Body.Close()
	output, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		return err
	}
	ocspResponse, err := ocsp.ParseResponse(output, issuerCert)
	if err != nil {
		return err
	}

	if ocspResponse.Status == ocsp.Revoked {
		return fmt.Errorf("certificate is revoked")
	}
	return nil
}

// isValidExpiry determines if a certificate is valid for an acceptable
// length of time per the CA/Browser Forum baseline requirements.
// See https://cabforum.org/wp-content/uploads/CAB-Forum-BR-1.3.0.pdf
func isValidExpiry(c *x509.Certificate) (int, int, bool) {
	mm := maxMonths(c.NotBefore)
	mv := monthsValid(c.NotBefore, c.NotAfter)
	return mm, mv, monthsValid(c.NotBefore, c.NotAfter) <= maxMonths(c.NotBefore)
}

func maxMonths(issued time.Time) int {
	if issued.After(sep2020) {
		return 13
	} else if issued.After(apr2015) {
		return 39
	} else if issued.After(jul2012) {
		return 60
	}
	return 120
}

func inclusiveDate(year int, month time.Month, day int) time.Time {
	return time.Date(year, month, day, 0, 0, 0, 0, time.UTC).Add(-1 * time.Nanosecond)
}

func monthsValid(issued, expiry time.Time) int {
	years := expiry.Year() - issued.Year()
	months := years*12 + int(expiry.Month()) - int(issued.Month())

	// Round up if valid for less than a full month
	if expiry.Day() > issued.Day() {
		months++
	}
	return months
}

func verifyServerCertificate(host string, certificates []*x509.Certificate) error {
	opts := x509.VerifyOptions{
		DNSName:       host,
		Intermediates: x509.NewCertPool(),
	}
	for _, cert := range certificates[1:] {
		opts.Intermediates.AddCert(cert)
	}

	_, err := certificates[0].Verify(opts)
	if err != nil {
		return err
	}

	switch certificates[0].PublicKey.(type) {
	case *rsa.PublicKey, *ecdsa.PublicKey, ed25519.PublicKey:
		break
	default:
		return fmt.Errorf("tls: server's certificate contains an unsupported type of public key: %T", certificates[0].PublicKey)
	}

	return err
}
