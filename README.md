# About

`checkssl` is a small SSL expiration tool.

# Install

Build from source:

    go install gitlab.com/ngerakines/checkssl

Install from source

# Usage

    $ checkssl -h
    Usage of checkssl:
      -host string
        	The server to connect to. (default "ngerakines.me")
      -port string
        	The port to connect to. (default "443")

## Example

    $ ./checkssl
    HOST ngerakines.me
    PORT 443
    RESOLVED 104.248.63.231
    FINGERPRINT d0a9bc653dd3b0b880fde14d49dee81f056cea0c8037452f8fcc53136e3d9650
    NAME *.ngerakines.me

# Docker

`checkssl` can be included in your docker tool container.

```Dockerfile
FROM ubuntu:20.04
WORKDIR /
RUN apt-get update; apt-get install -y unzip wget
RUN mkdir /checkssl-tmp/ && cd /checkssl-tmp/ \
    && wget -q https://gitlab.com/ngerakines/checkssl/uploads/efed32dde4591745025865b2f687d8e8/checkssl_1.0.0_linux_amd64.zip \
    && echo "2f32b9d9b1eb69d8c4229792d2b2daa1b7bb67745c96e2ce07b3f0e6f2254443  /checkssl-tmp/checkssl_1.0.0_linux_amd64.zip" > checksum \
    && sha256sum -c checksum \
    && unzip -qq checkssl_1.0.0_linux_amd64.zip -d ./ \
    && mv /checkssl-tmp/checkssl /usr/local/bin
CMD [ "checkssl" ]
```

In the above Dockerfile, checkssl is downloaded, verified, and placed /usr/local/bin/

    $ docker build .
    Sending build context to Docker daemon  2.127MB
    Step 1/5 : FROM ubuntu:20.04
     ---> 1d622ef86b13
    Step 2/5 : WORKDIR /
     ---> Using cache
     ---> 85cb82d2e410
    Step 3/5 : RUN apt-get update; apt-get install -y unzip wget
     ---> Using cache
     ---> aa2e975dc8ff
    Step 4/5 : RUN mkdir /checkssl-tmp/ && cd /checkssl-tmp/     && wget -q https://gitlab.com/ngerakines/checkssl/uploads/efed32dde4591745025865b2f687d8e8/checkssl_1.0.0_linux_amd64.zip     && echo "2f32b9d9b1eb69d8c4229792d2b2daa1b7bb67745c96e2ce07b3f0e6f2254443  /checkssl-tmp/checkssl_1.0.0_linux_amd64.zip" > checksum     && sha256sum -c checksum     && unzip -qq checkssl_1.0.0_linux_amd64.zip -d ./     && mv /checkssl-tmp/checkssl /usr/local/bin
     ---> Running in 9d5ac71caa78
    /checkssl-tmp/checkssl_1.0.0_linux_amd64.zip: OK
    Removing intermediate container 9d5ac71caa78
     ---> 89cc3ee603df
    Step 5/5 : CMD [ "checkssl" ]
     ---> Running in b7d25454d61a
    Removing intermediate container b7d25454d61a
     ---> 32ff8f595211
    Successfully built 32ff8f595211

The container can then be executed.

    $ docker run 32ff8f595211
    HOST ngerakines.me
    PORT 443
    RESOLVED 192.81.212.192
    FINGERPRINT d0a9bc653dd3b0b880fde14d49dee81f056cea0c8037452f8fcc53136e3d9650
    NAME *.ngerakines.me

